<div class="content cat_list">

    <section>
        <h1 id="estoque">Estoque:</h1>

        <?php

        //$data = filter_input_array(INPUT_POST, FILTER_DEFAULT);
        //if($data){
        //    require '_models/AdminEstoque.class.php';
        //    $create = new AdminEstoque();
            //$create->ExeCreate($id, $data);

        //    var_dump($id);
        //}   

        $empty = filter_input(INPUT_GET, 'empty', FILTER_VALIDATE_BOOLEAN);
        if ($empty):
            WSErro("Você tentou editar ou remover uma peça que não existe no sistema!", WS_INFOR);
        endif;

        $readEst = new Read;
        $readEst->ExeRead("estoque AS e JOIN produto AS p ON p.id=e.produtoid
                            JOIN categoria AS c ON p.categoriaid=c.id
                            JOIN fornecedor AS f ON p.fornecedorid=f.id
                            JOIN fabricante AS fab ON p.fabricanteid=fab.id", "WHERE e.produtoid=p.id AND p.ativo='1'", NULL, "e.*, p.codigo,p.nome, c.nome AS categoria, f.nome AS fornecedor,"
                . " fab.nome AS fabricante");
        ?>

        <div class="table-responsive">
            <table id="example" class="table table-striped table-hover table-sm">
                <thead class="thead-inverse">
                    <tr>
                        <th>Código</th>
                        <th>Nome</th>
                        <th>Categoria</th>
                        <th>Fabricante</th>
                        <th>Fornecedor</th>
                        <th>Quantidade</th>
                        <th>Mínimo</th>
                        <th>Ações</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (!$readEst->getResult()):
                        WSErro("Estoque zerado!", WS_INFOR);
                    else:
                        foreach ($readEst->getResult() as $key => $est):
                            extract($est);
                            ?>

                            <tr>
                                <td><?= $codigo; ?></td>
                                <td><?= $nome; ?></td>
                                <td data-toggle="tooltip" data-placement="bottom" title="<?= $categoria ?>">
                                    <?= (strlen($categoria) > 8) ? substr($categoria, 0, 8) . '...' : $categoria; ?></td>
                                <td data-toggle="tooltip" data-placement="bottom" title="<?= $fabricante ?>">
                                    <?= (strlen($fabricante) > 8) ? substr($fabricante, 0, 8) . '...' : $fabricante ?>
                                </td>
                                <td data-toggle="tooltip" data-placement="bottom" title="<?= $fornecedor ?>">
                                    <?= (strlen($fornecedor) > 8) ? substr($fornecedor, 0, 8) . '...' : $fornecedor; ?>
                                </td>
                                <td><?= $quantidade; ?></td>
                                <td><?= $minEstoque; ?></td>
                                <td>
                                    <a data-toggle="modal" data-target="#minModel" data-min="<?=$minEstoque?>" href="#" style="padding-left:25px">
                                        <img src="icons/25-fundo/edit.png" width="15" height="15"></a>
                                </td>
                        </tr>

                        <?php
                    endforeach;
                endif;
                ?>
                <tbody>
            </table>

            <div class="modal fade" id="minModel" tabindex="-1" role="dialog" aria-labelledby="minModelLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Alterar</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form name="PostForm" action="painel.php?exe=estoque/update&estid=<?= $id; ?>" method="post" enctype="multipart/form-data">
                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="minEstoqueRec" class="form-control-label" value="<?= $minEstoque; ?>">Minimo:</label>
                                    <input name="minEstoque" type="text" class="form-control" id="minEstoque">
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Salvar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
    </section>

    <div class="clear"></div>
</div> <!-- content home -->