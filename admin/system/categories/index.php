<div class="content cat_list">

    <section>

        <h1>Categorias:</h1>

        <?php
        $empty = filter_input(INPUT_GET, 'empty', FILTER_VALIDATE_BOOLEAN);
        if ($empty):
            WSErro("Você tentou editar ou remover uma categoria que não existe no sistema!", WS_INFOR);
        endif;
        ?>

        <?php
        $readSes = new Read;
        $readSes->ExeRead('ws_categories', "WHERE category_parent IS NULL ORDER BY category_title ASC");
        if (!$readSes->getResult()):
            WSErro("Não hà categorias cadastradas!", WS_INFOR);
        else:
            foreach ($readSes->getResult() as $ses):
                extract($ses);
                
                $readPosts = new Read;
                $readPosts->ExeRead("ws_posts", "WHERE post_cat_parent = :parent", "parent={$category_id}");
                
                $readCats = new Read;
                $readCats->ExeRead("ws_categories", "WHERE category_parent = :parent", "parent={$category_id}");
                
                $countSesPosts = $readPosts->getRowCount();
                $countSesCats = $readCats->getRowCount();
        ?>
        <section>

            <header>
                <h1><?= $category_title; ?>:  <span>( <?= $countSesPosts; ?> posts ) ( <?= $countSesCats; ?> Categorias )</span></h1>
                <p class="tagline"><?= $category_content; ?></p>

                <ul class="info post_actions">
                    <li><strong>Data:</strong> <?= date('d/m/Y H:i', strtotime($category_date)); ?>Hs</li>
                    <li><a class="act_view" target="_blank" href="painel.php?exe=posts/post&id=ID_DO_POST" title="Ver no site">Ver no site</a></li>
                    <li><a class="act_edit" href="painel.php?exe=categories/update&catid=<?= $category_id; ?>" title="Editar">Editar</a></li>
                    <li><a class="act_delete" href="painel.php?exe=categories/index&delete=<?= $category_id; ?>" title="Excluir">Deletar</a></li>
                </ul>
            </header>

            <h2>Sub categorias de <?= $category_title ?>:</h2>

            <?php
            $readSub = new Read;
            $readSub->ExeRead("ws_categories", "WHERE category_parent = :parent", "parent={$category_id}");
                        
            if(!$readSub->getResult()):
                WSErro("A seção <b>{$category_title}</b> ainda não tem sub categorias!", WS_INFOR);
            else:
                $a = 0;
                foreach ($readSub->getResult() as $cats):
                $a++;

                    $readsubPosts = new Read;
                    $readsubPosts->ExeRead("ws_posts", "WHERE post_category = :category", "category={$cats['category_id']}");
            
            ?>

                <article<?php if ($a % 3 == 0) echo ' class="right"'; ?>>
                    <h1><a target="_blank" href="../categories/<?= $cats['category_name'] ?>" title="Ver Categoria"><?= $cats['category_title'] ?>:</a>  ( <?= $readsubPosts->getRowCount(); ?> posts )</h1>

                    <ul class="info post_actions">
                        <li><strong>Data:</strong> <?= date('d/m/Y H:i', strtotime($cats['category_date'])); ?>Hs</li>
                        <li><a class="act_view" target="_blank" href="painel.php?exe=posts/post&id=ID_DO_POST" title="Ver no site">Ver no site</a></li>
                        <li><a class="act_edit" href="painel.php?exe=categories/update&catid=<?= $category_id; ?>" title="Editar">Editar</a></li>
                        <li><a class="act_delete" href="painel.php?exe=categories/index&delete=<?= $category_id; ?>" title="Excluir">Deletar</a></li>
                    </ul>
                </article>
                <?php
                endforeach;
                
                endif;
                ?>

        </section>
        <?php
        endforeach;
        endif;
        ?>

        <div class="clear"></div>
    </section>

    <div class="clear"></div>
</div> <!-- content home -->