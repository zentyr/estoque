<?php
if (!class_exists('Login')) :
    header('Location: ../../painel.php');
    die;
endif;
?>

<div class="col-8 center">

    <article>

        <header>
            <h1>Novo movimento:</h1>
        </header>

        <?php
        $data = filter_input_array(INPUT_POST, FILTER_DEFAULT);


        if (!empty($data['salvar'])):
            unset($data['salvar']);

            require '_models/AdminMovimento.class.php';
            $cadastra = new AdminMovimento;
            $cadastra->ExeCreate($data);

            if (!$cadastra->getResult()):
                WSErro($cadastra->getError()[0], $cadastra->getError()[1]);
            else:
                $readEstoque = new Read;
                $readEstoque->ExeRead('estoque', "WHERE id=:estid", "estid="<?= >)
                header('Location: painel.php?exe=movimentos/index&create=true&prodid=' . $cadastra->getResult());
            endif;
        elseif (!empty($data['cancel'])):
            header('Location: painel.php?exe=movimentos/index&create=false');
        endif;

        $readProd = new Read;
        $readProd->ExeRead('produto', "ORDER BY nome ASC");

        require 'form-insert-movimento.php';
        ?>
    </article>

    <div class="clear"></div>
</div> <!-- content home -->