<div class="content cat_list">

    <section>
        <h1 id="pecas">Movimentos:</h1>
        <a  href="painel.php?exe=movimentos/create">Novo</a>

        <?php
        $empty = filter_input(INPUT_GET, 'empty', FILTER_VALIDATE_BOOLEAN);
        if ($empty):
            WSErro("Você tentou editar ou remover uma peça que não existe no sistema!", WS_INFOR);
        endif;

        $readMov = new Read;
        $readMov->ExeRead(" movimentoitem AS mi join produto AS p ON mi.produtoid = p.id
		join movimento AS m ON mi.movimentoid = m.id
			join fabricante AS fab ON fab.id = p.fabricanteid
				join fornecedor AS forn ON forn.id = p.fornecedorid", "ORDER BY nome ASC", null, "m.tipo, p.nome, p.codigo, fab.nome AS fornecedor, 
				forn.nome AS fabricante, mi.quantidade, m.data, m.usuario ");
        ?>

        <div class="table-responsive">
            <table id="example" class="table table-striped table-hover table-sm">
                <thead class="thead-inverse">
                    <tr>
                        <th>Movimento</th>
                        <th>Peça</th>
                        <th>Código</th>
                        <th>Fabricante</th>
                        <th>Fornecedor</th>
                        <th>Quantidade</th>
                        <th>Data</th>
                        <th>Usuário</th>
                        <th>Ações</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (!$readMov->getResult()):
                        WSErro("Não hà peças cadastradas!", WS_INFOR);
                    else:
                        foreach ($readMov->getResult() as $key => $ses):
                            extract($ses);


                            // $readPecas->ExeRead("produto", "WHERE id = :id", "id={$id}");
                            ?>

                            <tr>
                                <td><?= ($tipo) ? "Entrada" : "Saida" ?></td>
                                <td data-toggle="modal" data-target="#flipFlop-<?= $key ?>"><?= $nome; ?></td>
                                <td><?= $codigo; ?></td>
                                <td data-toggle="tooltip" data-placement="bottom" title="<?= $fabricante ?>">
                                    <?= (strlen($fabricante) > 8) ? substr($fabricante, 0, 8) . '...' : $fabricante ?>
                                </td>
                                <td data-toggle="tooltip" data-placement="bottom" title="<?= $fornecedor ?>">
                                    <?= (strlen($fornecedor) > 8) ? substr($fornecedor, 0, 8) . '...' : $fornecedor; ?>
                                </td>
                                <td><?= $quantidade; ?></td>
                                <td><?= date("d/m/Y", strtotime($data)) ?></td>
                                <td><?= $usuario; ?></td>
                                <td>
                                    <a href="painel.php?exe=produtos/ativar&prodid=">
                                       
                                    </a>
                                    <a href="painel.php?exe=produtos/update&prodid="  style="padding-left:25px">
                                        <img src="icons/25-fundo/edit.png" width="15" height="15"></a>
                                    <a href="painel.php?exe=produtos/delete&prodid=" style="padding-left:25px">
                                        <img src="icons/delete25x25.png" width="15" height="15"></a>
                                </td>
                            </tr>

                            <?php
                        endforeach;
                    endif;
                    ?>
                <tbody>
            </table>
        </div>
        <div class="clear"></div>
    </section>

    <div class="clear"></div>
</div> <!-- content home -->