<div class="container">
    <form name="PostForm" action="" method="post" enctype="multipart/form-data">

        <!-- TIPO -->
        <div class="form-group row">
            <label for="nome" class="col-sm-2 col-form-label"><span class="field">Tipo</span> </label>
            <div class="col-sm-10">
                <select name="categoriaid"  class="form-control col-4 mb-2 mb-sm-0" >
                    <option value="null"> Selecione: </option>
                    <option value="1" selected="selected"> Entrada </option>
                    <option value="0"> Saída </option>
                </select>
            </div>
        </div>
        <!-- PRODUTOS -->

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Peça:</label>
            <div class="col-sm-10 input-group">
                <table id="" class="dynatable">
                    <thead>
                        <tr>
                            <th>Código</th>
                            <th>Quantidade</th>
                            <th>Valor uni</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="prototype">
                            <td>
                                <select name="produtoArray[]">
                                    <option value="null"> Selecione: </option>

                                    <?php
                                    if (!$readProd->getResult()):
                                        echo "<option disabled=\"disabled\" value=\"null\"> Cadastre antes uma peça! </option>";
                                    else:
                                        foreach ($readProd->getResult() as $pro):
                                            echo "<option value=\"{$pro['id']}\" ";
                                            if ($pro['id'] == $data['produtoArray']):
                                                echo ' selected="selected" ';
                                            endif;
                                            echo "> {$pro['codigo']} </option>";
                                        endforeach;
                                    endif;
                                    ?>
                                </select>

                            </td>
                            <td><input type="text" name="quantidade[]"/></td>
                            <td><input type="text" name="valor[]"/></td>
                            <td><input type="button" class="add" value="Add"></td>
                            <td><input type="button" class="remove" value="Remove"></td>
                        </tr>
                    </tbody>
                </table>
                <!-- Este botão quando acionado irá chamar a função responsável em inserir a linha na tabela -->
            </div>
        </div>


        <input type="submit" class="btn btn-danger" name="cancel" id="cancel" value="Cancelar" />
        <input type="submit" class="btn btn-success" name="salvar" id="salvar" value="Salvar" />
    </form>
</div>