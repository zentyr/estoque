<?php
if (!class_exists('Login')) :
    header('Location: ../../painel.php');
    die;
endif;
?>

<div class="col-8 center">

    <article>

        <header>
            <h1>Inserir peça:</h1>
        </header>

        <?php
        $data = filter_input_array(INPUT_POST, FILTER_DEFAULT);
        
        if(isset($data['addcategoria'])):
            require '_models/AdminCategoria.class.php';
            $create = new AdminCategoria();
            $create->ExeCreate($data);
            
            $data['categoriaid'] = $create->getResult();
                
        elseif(isset($data['addfabricante'])):
            require '_models/AdminFabricante.class.php';
            $create = new AdminFabricante();
            $create->ExeCreate($data);
            
            $data['fabricanteid'] = $create->getResult();
            
        elseif(isset($data['addfornecedor'])):
            require '_models/AdminFornecedor.class.php';
            $create = new AdminFornecedor();
            $create->ExeCreate($data);
            
            $data['fornecedorid'] = $create->getResult();
            
        elseif (!empty($data['salvar'])):
            unset($data['salvar']);
            unset($data['nomeNovaCategoria']);
            unset($data['nomeNovoFabricante']);
            unset($data['nomeNovoFornecedor']);

            $file = $_FILES['arquivo'];
            if ($file):
                $upload = new Upload("images/");
                $upload->Image($file, $data['nome'], null, "produtos");
                if ($upload->getResult()):
                    $data['arquivo'] = $upload->getName();
                else:
                    $data['arquivo'] = null;
                endif;
            endif;

            require '_models/AdminProduto.class.php';
            $cadastra = new AdminProduto;
            $cadastra->ExeCreate($data);

            if (!$cadastra->getResult()):
                WSErro($cadastra->getError()[0], $cadastra->getError()[1]);
            else:
                header('Location: painel.php?exe=produtos/index&create=true&prodid=' . $cadastra->getResult());

            endif;
        elseif (!empty($data['salvarincluir'])):
            unset($data['salvarincluir']);
            unset($data['nomeNovaCategoria']);
            unset($data['nomeNovoFabricante']);
            unset($data['nomeNovoFornecedor']);

            $file = $_FILES['arquivo'];
            if ($file):
                $upload = new Upload("images/");
                $upload->Image($file, $data['nome'], null, "produtos");
                if ($upload->getResult()):
                    $data['arquivo'] = $upload->getName();
                else:
                    $data['arquivo'] = null;
                endif;
            endif;

            require '_models/AdminProduto.class.php';
            $cadastra = new AdminProduto;
            $cadastra->ExeCreate($data);

            if (!$cadastra->getResult()):
                WSErro($cadastra->getError()[0], $cadastra->getError()[1]);
            else:
                header('Location: painel.php?exe=produtos/create&create=true');
            endif;
        elseif (!empty($data['cancel'])):
            header('Location: painel.php?exe=produtos/index&create=false');
        endif;
        
        require 'form-insert-produto.php';
        ?>
    </article>

    <div class="clear"></div>
</div> <!-- content home -->