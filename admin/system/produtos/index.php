<div class="content cat_list">

    <section>
        <h1 id="pecas">Peças:</h1>

        <?php
        $empty = filter_input(INPUT_GET, 'empty', FILTER_VALIDATE_BOOLEAN);
        if ($empty):
            WSErro("Você tentou editar ou remover uma peça que não existe no sistema!", WS_INFOR);
        endif;

        $readSes = new Read;
        $readSes->ExeRead("produto INNER JOIN categoria on produto.categoriaid = categoria.id INNER JOIN fabricante on produto.fabricanteid = fabricante.id"
                . " INNER JOIN fornecedor ON produto.fornecedorid = fornecedor.id", "ORDER BY nome ASC", null, "produto.id, produto.nome, categoria.nome AS categoria, produto.codigo,"
                . "produto.ativo, produto.descricao, fornecedor.nome AS fornecedor,"
                . "fabricante.nome as fabricante, produto.arquivo");
        ?>

        <div class="table-responsive">
            <table id="example" class="table table-striped table-hover table-sm">
                <thead class="thead-inverse">
                    <tr>
                        <th>Nome</th>
                        <th>Ativo</th>
                        <th>Categoria</th>
                        <th>Código</th>
                        <th>Descrição</th>
                        <th>Fabricante</th>
                        <th>Fornecedor</th>
                        <th>Ações</th>
                    </tr>
                </thead>
                <tbody>
                    <?php

                    if (!$readSes->getResult()):
                        WSErro("Não hà peças cadastradas!", WS_INFOR);
                    else:
                        foreach ($readSes->getResult() as $key => $ses):
                            extract($ses);


                           // $readPecas->ExeRead("produto", "WHERE id = :id", "id={$id}");
                            ?>

                            <tr>
                                <td data-toggle="modal" data-target="#flipFlop-<?= $key ?>"><?= $nome; ?></td>

                                <td><?= ($ativo) ? "Sim" : "Não" ?></td>
                                <td data-toggle="tooltip" data-placement="bottom" title="<?= $categoria ?>">
                                    <?= (strlen($categoria) > 8) ? substr($categoria, 0, 8) . '...' : $categoria; ?>
                                </td>
                                <td><?= $codigo; ?></td>
                                <td data-toggle="tooltip" data-placement="bottom" title="<?= strip_tags($descricao) ?>">
                                    <?= (strlen(strip_tags($descricao)) > 8) ? substr(strip_tags($descricao), 0, 8) . '...' : strip_tags($descricao) ?>
                                </td>
                                <td data-toggle="tooltip" data-placement="bottom" title="<?= $fabricante ?>">
                                    <?= (strlen($fabricante) > 8) ? substr($fabricante, 0, 8) . '...' : $fabricante ?>
                                </td>
                                <td data-toggle="tooltip" data-placement="bottom" title="<?= $fornecedor ?>">
                                    <?= (strlen($fornecedor) > 8) ? substr($fornecedor, 0, 8) . '...' : $fornecedor; ?>
                                </td>
                                <td>
                                    <a href="painel.php?exe=produtos/ativar&prodid=<?= $id ?>">
                                        <?php
                                        if ($ativo):
                                            echo "<img src=\"icons/25-fundo/mini_switch-off-icon.png\" width=\"15\" height=\"15\">";
                                        else:
                                            echo "<img src=\"icons/25-fundo/mini_switch-on-icon.png\" width=\"15\" height=\"15\">";
                                        endif;
                                        ?>
                                    </a>
                                    <a href="painel.php?exe=produtos/update&prodid=<?= $id ?>"  style="padding-left:25px">
                                        <img src="icons/25-fundo/edit.png" width="15" height="15"></a>
                                    <a href="painel.php?exe=produtos/delete&prodid=<?= $id ?>" style="padding-left:25px">
                                        <img src="icons/delete25x25.png" width="15" height="15"></a>
                                </td>

                                <!-- The modal -->
                        <div class="modal fade" id="flipFlop-<?= $key ?>" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <h4 class="modal-title" id="modalLabel"><?= $nome; ?></h4>
                                    </div>

                                    <div class="modal-body">
                                        <?php
                                        if (is_null($arquivo)):
                                            echo '<img class"center" src="icons/original/png/image.png" width="200" height="200"/>';
                                        else:
                                            echo '<img align="center" src="images/produtos/' . $arquivo . '" width="200" height="200"/>';
                                        endif;
                                        ?>

                                        <div class="center">

                                            <p>Ativo: <?= ($ativo == 1) ? "Sim" : "Não" ?></p>
                                            <p>Categoria: <?= $categoria ?></p>
                                            Código: <?= $codigo ?><br>
                                            Fabricante: <a href="painel.php?exe=fabricante/view&fabid=<?= $fabricante ?>"><?= $fabricante ?></a><br>
                                            Fornecedor: <a href="painel.php?exe=fornecedor/view&forid=<?= $fornecedor ?>"><?= $fornecedor ?></a><br>
                                            Descrição: <?= $descricao ?><br>

                                        </div>
                                    </div>

                                    <div class="modal-footer">
                                        <span><a class="btn red" href="painel.php?exe=produtos/ativar&prodid=<?= $id ?>"><?= ($ativo) ? "Desativar" : "Ativar" ?></a></span>
                                        <span><a class="btn red" href="painel.php?exe=produtos/delete&prodid=<?= $id ?>">Excluir</a></span>
                                        <span><a class="btn blue" href="painel.php?exe=produtos/update&prodid=<?= $id ?>">Alterar</a></span>
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        </tr>

                        <?php
                    endforeach;
                endif;
                ?>
                <tbody>
            </table>
        </div>
        <div class="clear"></div>
    </section>

    <div class="clear"></div>
</div> <!-- content home -->