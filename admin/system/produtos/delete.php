<?php
if (!class_exists('Login')) :
    header('Location: ../../painel.php');
    die;
endif;
?>

<div class="content form_create">

    <article>

        <header>
            <h1>Deletar Produto:</h1>
        </header>

        <?php
        require '_models/AdminProduto.class.php';
        $delete = new AdminProduto;
        $prodid = filter_input(INPUT_GET, 'prodid', FILTER_VALIDATE_INT);
      
        $delete->ExeDelete($prodid);

        header('Location: painel.php?exe=produtos/index#pecas');
        ?>

</div> <!-- content home -->