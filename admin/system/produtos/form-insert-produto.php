<div class="container">
    <form name="PostForm" action="" method="post" enctype="multipart/form-data">

        <!-- CATEGORIA -->
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Categoria:</label>
            <div class="col-sm-10 input-group">
                <select name="categoriaid"  class="form-control col-4 mb-2 mb-sm-0">
                    <option value="null"> Selecione: </option>

                    <?php
                    $readCat = new Read;
                    $readCat->ExeRead('categoria', "ORDER BY nome ASC");
                    if (!$readCat->getResult()):
                        echo "<option disabled=\"disabled\" value=\"null\"> Cadastre antes uma categoria! </option>";
                    else:
                        foreach ($readCat->getResult() as $cat):
                            echo "<option value=\"{$cat['id']}\" ";
                            if ($cat['id'] == $data['categoriaid']):
                                echo ' selected="selected" ';
                            endif;
                            echo "> {$cat['nome']} </option>";
                        endforeach;
                    endif;
                    ?>
                </select>
                <span class="input-group-btn">
                    <a href="#flipFlop-addCategoria" data-toggle="modal"><button class="btn btn-secondary" type="button">+</button></a>
                </span>
            </div>
        </div>

        <!-- NOME -->
        <div class="form-group row">
            <label for="nome" class="col-sm-2 col-form-label"><span class="field">Nome:</span> </label>
            <div class="col-sm-10">
                <input type="text" class="form-control form-control-sm col-4 mr-sm-2 mb-2 mb-sm-0" name="nome" id="nome" value="<?= (isset($data['nome'])) ? $data['nome'] : "" ?>" />
            </div>
        </div>

        <!-- ATIVO -->
        <div class="form-group row">
            <label class ="col-sm-2 col-form-label" ><span>Ativo:</span> </label>
            <div class="col-sm-10">
                <select name="ativo" value="null" class="form-control col-4 mr-sm-2 mb-2 mb-sm-0">
                    <option value="null"> Selecione: </option>

                    <?php
                    echo "<option value='1' ";
                    if ($data['ativo'] == 1):
                        echo ' selected="selected" ';
                    endif;
                    echo "> SIM </option>";

                    echo "<option value='0' ";
                    if ($data['ativo'] == 0):
                        echo ' selected="selected" ';
                    endif;
                    echo "> NÃO </option>";
                    ?>
                </select>
            </div>
        </div>

        <!-- CODIGO -->
        <div class="form-group row">
            <label class="col-sm-2 col-form-label" for="nome"><span class="field" style="width: 300px">Código:</span></label>
            <div class="col-sm-10">
                <input type="text" name="codigo" class="form-control form-control-sm col-4 mr-sm-2 mb-2 mb-sm-0" id="codigo" value="<?= (isset($data['codigo'])) ? $data['codigo'] : "" ?>"/>
            </div>
        </div>

        <!-- FABRICANTE -->
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Fabricante:</label>
            <div class="col-sm-10 input-group">
                <select name="fabricanteid" class="form-control col-4 mb-2 mb-sm-0">
                    <option value="null"> Selecione: </option>

                    <?php
                    $readFab = new Read;
                    $readFab->ExeRead('fabricante', "ORDER BY nome ASC");
                    if (!$readFab->getResult()):
                        echo "<option disabled=\"disabled\" value=\"null\"> Cadastre antes um fabricante! </option>";
                    else:
                        foreach ($readFab->getResult() as $fab):
                            echo "<option value=\"{$fab['id']}\" ";
                            if ($fab['id'] == $data['fabricanteid']):
                                echo ' selected="selected" ';
                            endif;
                            echo "> {$fab['nome']} </option>";
                        endforeach;
                    endif;
                    ?>
                </select>
                <span class="input-group-btn">
                    <a href="#flipFlop-addFabricante" data-toggle="modal"><button class="btn btn-secondary" type="button">+</button></a>
                </span>
            </div>
        </div>

        <!-- FORNECEDOR -->
        <div class="form-group row">
            <label class="col-sm-2 col-form-label"><span class="field">Fornecedor:</span></label>
            <div class="col-sm-10 input-group">
                <select name="fornecedorid" class="form-control col-4 mb-2 mb-sm-0">
                    <option value="null"> Selecione: </option>

                    <?php
                    $readFor = new Read;
                    $readFor->ExeRead('fornecedor', "ORDER BY nome ASC");
                    if (!$readFor->getResult()):
                        echo "<option disabled=\"disabled\" value=\"null\"> Cadastre antes um fornecedor! </option>";
                    else:
                        foreach ($readFor->getResult() as $for):
                            echo "<option value=\"{$for['id']}\" ";
                            if ($for['id'] == $data['fornecedorid']):
                                echo ' selected="selected" ';
                            endif;
                            echo "> {$for['nome']} </option>";
                        endforeach;
                    endif;
                    ?>
                </select>
                <span class="input-group-btn">
                    <a href="#flipFlop-addFornecedor" data-toggle="modal"><button class="btn btn-secondary" type="button">+</button></a>
                </span>
            </div>
        </div>

        <!-- DESCRIÇÃO -->
        <div class="form-group row">
            <label class="col-sm-2 col-form-label"><span class="field">Descrição:</span></label>
            <div class="col-sm-10">
                <textarea name="descricao" class="form-control form-control-sm col-4 mr-sm-2 mb-2 mb-sm-0" rows="5" value=""><?= (isset($data['descricao'])) ? $data['descricao'] : "" ?></textarea>
            </div>
        </div>

        <!-- FOTO -->
        <div class="form-group row">
            <label class="col-sm-2 col-form-label"><span class="field">Foto:</span></label>
            <div class="col-sm-10">
                <input type="file" name="arquivo" class="form-control-file form-control-sm col-4 mr-sm-2 mb-2 mb-sm-0"/>
            </div>
        </div>


        <input type="submit" class="btn btn-danger" name="cancel" id="cancel" value="Cancelar" />
        <input type="submit" class="btn btn-success" name="salvar" id="salvar" value="Salvar" />
        <input type="submit" class="btn btn-info" name="salvarincluir" id="salvarincluir" value="Salvar e Inserir novo" />

        <!-- MODALS -->
        <!-- CATEGORIA -->
        <div class="modal fade" id="flipFlop-addCategoria" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="modalLabelCategoria">Adicionar Categoria</h4>
                    </div>

                    <div class="modal-body">

                        <div class="center">
                            <input type="text" name="nomeNovaCategoria" id="nomeNovaCategoria">
                            <button name="addcategoria" id="addcategoria" type="submit">Criar</button>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- FABRICANTE -->
        <div class="modal fade" id="flipFlop-addFabricante" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="modalLabelFabricante">Adicionar Fabricante</h4>
                    </div>

                    <div class="modal-body">

                        <div class="center">
                            <input type="text" name="nomeNovoFabricante" id="nomeNovoFabricante">
                            <button name="addfabricante" id="addfabricante" type="submit">Criar</button>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- FORNECEDOR -->
        <div class="modal fade" id="flipFlop-addFornecedor" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="modalLabelFornecedor">Adicionar Categoria</h4>
                    </div>

                    <div class="modal-body">

                        <div class="center">
                            <input type="text" name="nomeNovoFornecedor" id="nomeNovoFornecedor">
                            <button name="addfornecedor" id="addfornecedor" type="submit">Criar</button>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MODALS -->
    </form>
</div>