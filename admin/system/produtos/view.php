<?php
if (!class_exists('Login')) :
    header('Location: ../../painel.php');
    die;
endif;
?>

<div class="content form_create">

    <article>

        <?php
        $prodid = filter_input(INPUT_GET, 'prodid', FILTER_VALIDATE_INT);
        ?>
        <?php
        $readProd = new Read;
        $readProd->ExeRead("produto", "WHERE id = :id", "id={$prodid}");
        extract($readProd->getResult()[0]);

        $readCat = new Read;
        $readCat->ExeRead("categoria", "WHERE id = :catid", "catid={$categoriaid}");
        $categoria = $readCat->getResult()[0]['nome'];

        $readFab = new Read;
        $readFab->ExeRead("fabricante", "WHERE id = :fabid", "fabid={$fabricanteid}");
        $fabricante = $readFab->getResult()[0]['nome'];

        $readFor = new Read;
        $readFor->ExeRead("fornecedor", "WHERE id = :forid", "forid={$fornecedorid}");
        $fornecedor = $readFor->getResult()[0]['nome'];
        ?>

        <h1><?= $readProd->getResult()[0]['nome'] ?></h1>

        <?php
        if (is_null($readProd->getResult()[0]['arquivo'])):
            echo '<img src="icons/original/png/image.png" width="200" height="200"/>';
        else:
            echo '<img src="images/produtos/' . $readProd->getResult()[0]['arquivo'] . '" width="200" height="200"/>';
        endif;
        ?>


        <p>
            Ativo: <?= ($ativo == 1) ? "Sim" : "Não" ?><br>
            Categoria: <?= $categoria ?><br>
            Código: <?= $codigo ?><br>
            Fabricante: <a href="painel.php?exe=fabricante/view&fabid=<?= $fabricanteid ?>"><?= $fabricante ?></a><br>
            Fornecedor: <a href="painel.php?exe=fornecedor/view&forid=<?= $fornecedorid ?>"><?= $fornecedor ?></a><br>
            Descrição: <?= $descricao ?><br>
        </p>

        <div class="center">
            <span><a class="btn blue" href="painel.php?exe=produtos/update&prodid=<?= $id ?>">Alterar</a></span>
            <span><a class="btn blue" href="painel.php?exe=produtos/index#pecas">Voltar</a></span>
            <span><a class="btn red" href="painel.php?exe=produtos/delete&prodid=<?= $prodid ?>">Excluir</a></span>
        </div>

    </article>

    <div class="clear"></div>
</div> <!-- content home -->