<div class="content cat_list">

    <section>
        <h1 id="pecas">Peças:</h1>

        <?php
        $empty = filter_input(INPUT_GET, 'empty', FILTER_VALIDATE_BOOLEAN);
        if ($empty):
            WSErro("Você tentou editar ou remover uma peça que não existe no sistema!", WS_INFOR);
        endif;

        $query = "";
        $pesquisa = filter_input_array(INPUT_POST, FILTER_DEFAULT);
        if (!empty($pesquisa['pesquisar'])):
            $query = " WHERE produto.nome LIKE \"%{$pesquisa['search']}%\" OR "
                    . "produto.descricao LIKE \"%{$pesquisa['search']}%\" OR "
                    . "categoria.nome LIKE \"%{$pesquisa['search']}%\" OR "
                    . "fornecedor.nome LIKE \"%{$pesquisa['search']}%\" OR "
                    . "fabricante.nome LIKE \"%{$pesquisa['search']}%\" OR "
                    . "produto.codigo LIKE \"%{$pesquisa['search']}%\" ORDER BY produto.nome ASC";
        else:
            $query = "ORDER BY nome ASC";
        endif;

        $readSes = new Read;
        $readSes->ExeRead("produto INNER JOIN categoria on produto.categoriaid = categoria.id INNER JOIN fabricante on produto.fabricanteid = fabricante.id"
                . " INNER JOIN fornecedor ON produto.fornecedorid = fornecedor.id", $query, null, "produto.id, produto.nome, categoria.nome AS categoria, produto.codigo,"
                . "produto.ativo, produto.descricao, fornecedor.nome AS fornecedor,"
                . "fabricante.nome as fabricante, produto.arquivo");



        ?>

        <div class="table-responsive">
            <table id="example" class="table table-striped">
                <thead>
                    <tr>
                        <th>Nome</th>
                        <th>Ativo</th>
                        <th>Categoria</th>
                        <th>Código</th>
                        <th>Descrição</th>
                        <th>Fabricante</th>
                        <th>Fornecedor</th>
                    </tr>
                </thead>
                <tbody>
                    <?php

                    if (!$readSes->getResult()):
                        WSErro("Não hà peças cadastradas!", WS_INFOR);
                    else:
                        foreach ($readSes->getResult() as $key => $ses):
                            extract($ses);


                           // $readPecas->ExeRead("produto", "WHERE id = :id", "id={$id}");
                            ?>

                            <tr>
                                <td data-toggle="modal" data-target="#flipFlop-<?= $key ?>"><?= $nome; ?></td>

                                <td><?= ($ativo) ? "Sim" : "Não" ?></td>
                                <td><?= $categoria; ?></td>
                                <td><?= $codigo; ?></td>
                                <td><?= strip_tags($descricao) ?></td>
                                <td><?= $fabricante ?></td>
                                <td><?= $fornecedor; ?></td>
                                </tr>

                        <?php
                    endforeach;
                endif;
                ?>
                <tbody>
            </table>
        </div>
        <div class="clear"></div>
    </section>

    <div class="clear"></div>
</div> <!-- content home -->