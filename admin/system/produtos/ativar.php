<?php
if (!class_exists('Login')) :
    header('Location: ../../painel.php');
    die;
endif;
?>

<div class="content form_create">

    <article>

        <header>
            <h1>Ativar Produto:</h1>
        </header>

        <?php
        $prodid = filter_input(INPUT_GET, 'prodid', FILTER_VALIDATE_INT);
        
        require '_models/AdminProduto.class.php';
        $ativa = new AdminProduto;
        
        $read = new Read;
        $read->ExeRead('produto', "WHERE id = :prodid", "prodid={$prodid}");
        
        if($read->getResult()[0]['ativo'] == 1):
            $ativa->ExeUpdate($prodid, array('ativo' => '0', 'nome' => $read->getResult()[0]['nome']));
        
        else:
            $ativa->ExeUpdate($prodid, array('ativo' => '1',  'nome' => $read->getResult()[0]['nome']));
        endif;
        
        header('Location: painel.php?exe=produtos/index#pecas');
        ?>
    </article>

    <div class="clear"></div>
</div> <!-- content home -->