<?php
if (!class_exists('Login')) :
    header('Location: ../../painel.php');
    die;
endif;
?>

<div class="content form_create">

    <article>

        <header>
            <h1>Atualizar Produto:</h1>
        </header>

        <?php
        
        $data = filter_input_array(INPUT_POST, FILTER_DEFAULT);

        $prodid = filter_input(INPUT_GET, 'prodid', FILTER_VALIDATE_INT);
        
        if(isset($data['addcategoria'])):
            require '_models/AdminCategoria.class.php';
            $create = new AdminCategoria();
            $create->ExeCreate($data);
            
            $data['categoriaid'] = $create->getResult();
                
        elseif(isset($data['addfabricante'])):
            require '_models/AdminFabricante.class.php';
            $create = new AdminFabricante();
            $create->ExeCreate($data);
            
            $data['fabricanteid'] = $create->getResult();
            
        elseif(isset($data['addfornecedor'])):
            require '_models/AdminFornecedor.class.php';
            $create = new AdminFornecedor();
            $create->ExeCreate($data);
            
            $data['fornecedorid'] = $create->getResult();
        elseif (!empty($data['salvar'])):
            unset($data['salvar']);
            unset($data['nomeNovaCategoria']);
            unset($data['nomeNovoFabricante']);
            unset($data['nomeNovoFornecedor']);

            require '_models/AdminProduto.class.php';
            $cadastra = new AdminProduto;
            $cadastra->ExeUpdate($prodid, $data);
            WSErro($cadastra->getError()[0], $cadastra->getError()[1]);
            header('Location: painel.php?exe=produtos/index&create=true#pecas');

        elseif (!empty($data['salvarincluir'])):
            unset($data['salvarincluir']);
            unset($data['nomeNovaCategoria']);
            unset($data['nomeNovoFabricante']);
            unset($data['nomeNovoFornecedor']);

            require '_models/AdminProduto.class.php';
            $cadastra = new AdminProduto;
            $cadastra->ExeUpdate($prodid, $data);

            if (!$cadastra->getResult()):
                WSErro($cadastra->getError()[0], $cadastra->getError()[1]);
            else:
                header('Location: painel.php?exe=produtos/create&create=true#pecas');
            endif;

        elseif (!empty($data['cancel'])):
            header('Location: painel.php?exe=produtos/index&create=false#pecas');

        else:
            $read = new Read;
            $read->ExeRead('produto', "WHERE id = :id", "id={$prodid}");
            if (!$read->getResult()):
                header('Location: painel.php?exe=categories/index&empty=true#pecas');
            else:
                $data = $read->getResult()[0];
            endif;
        endif;
        
        require 'form-insert-produto.php';
        ?>
    </article>

    <div class="clear"></div>
</div> <!-- content home -->