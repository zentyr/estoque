<?php
if (!class_exists('Login')) :
    header('Location: ../../painel.php');
    die;
endif;
?>

<div class="content form_create">

    <article>

        <header>
            <h1>Atualizar Produto:</h1>
        </header>

        <?php
        $data = filter_input_array(INPUT_POST, FILTER_DEFAULT);
        $prodid = filter_input(INPUT_GET, 'prodid', FILTER_VALIDATE_INT);

        if (!empty($data['salvar'])):
            unset($data['salvar']);

            require '_models/AdminProduto.class.php';
            $cadastra = new AdminProduto;
            $cadastra->ExeUpdate($prodid, $data);

            WSErro($cadastra->getError()[0], $cadastra->getError()[1]);
            header('Location: painel.php?exe=produtos/index&create=true');

        elseif (!empty($data['salvarincluir'])):
            unset($data['salvarincluir']);

            require '_models/AdminProduto.class.php';
            $cadastra = new AdminProduto;
            $cadastra->ExeUpdate($prodid, $data);

            if (!$cadastra->getResult()):
                WSErro($cadastra->getError()[0], $cadastra->getError()[1]);
            else:
                header('Location: painel.php?exe=produtos/create&create=true');
            endif;

        elseif (!empty($data['cancel'])):
            header('Location: painel.php?exe=produtos/index&create=false');

        else:
            $read = new Read;
            $read->ExeRead('produto', "WHERE id = :id", "id={$prodid}");
            if (!$read->getResult()):
                header('Location: painel.php?exe=categories/index&empty=true');
            else:
                $data = $read->getResult()[0];
            endif;
        endif;

//        $checkCreate = filter_input(INPUT_GET,'create', FILTER_VALIDATE_BOOLEAN);
//        if($checkCreate):
//            $tipo = (empty($data['category_parent']) ? 'seção' : 'categoria');
//            WSErro("A {$tipo} <b>{$data['category_title']}</b> foi cadastrada com sucesso no sistema! Continue atualizando a mesma!", WS_ACCEPT);
//        endif;
        ?>


        <form name="PostForm" action="" method="post" enctype="multipart/form-data">
            <label class="label">
                <span class="field">Categoria:</span>
                <select name="categoriaid">
                    <option value="null"> Selecione: </option>

                    <?php
                    $readCat = new Read;
                    $readCat->ExeRead('categoria', "ORDER BY nome ASC");
                    if (!$readCat->getResult()):
                        echo "<option disabled=\"disabled\" value=\"null\"> Cadastre antes uma categoria! </option>";
                    else:
                        foreach ($readCat->getResult() as $cat):
                            echo "<option value=\"{$cat['id']}\" ";
                            if ($cat['id'] == $data['categoriaid']):
                                echo ' selected="selected" ';
                            endif;
                            echo "> {$cat['nome']} </option>";
                        endforeach;
                    endif;
                    ?>
                </select>
            </label>

            <label class="label" for="nome"><span class="field">Nome:</span>
                <input type="text" name="nome" id="nome" value="<?php if (isset($data)) echo $data['nome']; ?>" />
            </label>

            <label class="label">
                <span class="field">Ativo:</span>
                <select name="ativo">
                    <option value="null"> Selecione: </option>

                    <?php
                    $ativo = 1;
                    echo "<option value=1 ";
                    if ($data['ativo'] == 1):
                        echo ' selected="selected" ';
                    endif;
                    echo "> SIM </option>";

                    echo "<option value=0 ";
                    if ($data['ativo'] == 0):
                        echo ' selected="selected" ';
                    endif;
                    echo "> NÃO </option>";
                    ?>
                </select>
            </label>
            
            <label class="label" for="nome" style="width: 300px"><span class="field" style="width: 300px">Código:</span>
                <input type="text" name="codigo" id="codigo" value="<?php if (isset($data)) echo $data['codigo']; ?>"/>
            </label>

            <label class="label">
                <span class="field">Fabricante:</span>
                <select name="fabricanteid">
                    <option value="null"> Selecione: </option>

                    <?php
                    $readFab = new Read;
                    $readFab->ExeRead('fabricante', "ORDER BY nome ASC");
                    if (!$readFab->getResult()):
                        echo "<option disabled=\"disabled\" value=\"null\"> Cadastre antes um fabricante! </option>";
                    else:
                        foreach ($readFab->getResult() as $fab):
                            echo "<option value=\"{$fab['id']}\" ";
                            if ($fab['id'] == $data['fabricanteid']):
                                echo ' selected="selected" ';
                            endif;
                            echo "> {$fab['nome']} </option>";
                        endforeach;
                    endif;
                    ?>
                </select>
            </label>

            <label class="label" style="width: 300px">
                <span class="field">Fornecedor:</span>
                <select name="fornecedorid">
                    <option value="null"> Selecione: </option>

                    <?php
                    $readFor = new Read;
                    $readFor->ExeRead('fornecedor', "ORDER BY nome ASC");
                    if (!$readFor->getResult()):
                        echo "<option disabled=\"disabled\" value=\"null\"> Cadastre antes um fornecedor! </option>";
                    else:
                        foreach ($readFor->getResult() as $for):
                            echo "<option value=\"{$for['id']}\" ";
                            if ($for['id'] == $data['fornecedorid']):
                                echo ' selected="selected" ';
                            endif;
                            echo "> {$for['nome']} </option>";
                        endforeach;
                    endif;
                    ?>
                </select>
            </label>

            <label>
                <span class="field">Descrição:</span>
                <textarea name="descricao" rows="5"><?php if (isset($data)) echo $data['descricao']; ?></textarea>
            </label>

            <!--<div class="gbform"></div>-->

            <input type="submit" class="btn red" name="cancel" id="cancel" value="Cancelar" />
            <input type="submit" class="btn green" name="salvar" id="salvar" value="Salvar" />
            <input type="submit" class="btn blue" name="salvarincluir" id="salvarincluir" value=" e Inserir novo" />

        </form>

    </article>

    <div class="clear"></div>
</div> <!-- content home -->