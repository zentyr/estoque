<div class="content cat_list">

    <section>
        <h1>Peças:</h1>

        <?php
        $empty = filter_input(INPUT_GET, 'empty', FILTER_VALIDATE_BOOLEAN);
        if ($empty):
            WSErro("Você tentou editar ou remover uma peça que não existe no sistema!", WS_INFOR);
        endif;

        $query = "";
        $pesquisa = filter_input_array(INPUT_POST, FILTER_DEFAULT);
        if (!empty($pesquisa['pesquisar'])):
            $query = " WHERE produto.nome LIKE \"%{$pesquisa['search']}%\" OR "
                    . "produto.descricao LIKE \"%{$pesquisa['search']}%\" OR "
                    . "categoria.nome LIKE \"%{$pesquisa['search']}%\" OR "
                    . "fornecedor.nome LIKE \"%{$pesquisa['search']}%\" OR "
                    . "fabricante.nome LIKE \"%{$pesquisa['search']}%\" OR "
                    . "produto.codigo LIKE \"%{$pesquisa['search']}%\" ORDER BY produto.nome ASC";
        else:
            $query = "ORDER BY nome ASC";
        endif;
        ?>

        <form action="" method="post" enctype="multipart/form-data" class="right">
            <label class="label" for="search">
                <input type="text" name="search" id="nome" value="<?php if (isset($pesquisa)) echo $pesquisa['search']; ?>" />
            </label>
            <input type="submit" class="btn blue" name="pesquisar" value="Pesquisar" />
        </form>

        <table width="844">
            <tr>
                <td>Nome</td>
                <td>Ativo</td>
                <td>Categoria</td>
                <td><a>Código</a></td>
                <td><a>Descrição</a></td>
                <td><a>Fabricante</a></td>
                <td><a>Fornecedor</a></td>
            </tr>
            <?php
            $readCat = new Read;
            $readPecas = new Read;
            $readFor = new Read;
            $readFab = new Read;

            $readSes = new Read;
            $readSes->ExeRead("produto INNER JOIN categoria on produto.categoriaid = categoria.id INNER JOIN fabricante on produto.fabricanteid = fabricante.id"
                    . " INNER JOIN fornecedor ON produto.fornecedorid = fornecedor.id", $query, null, "produto.id, produto.nome, categoria.nome AS categoria, produto.codigo,"
                    . "produto.ativo, produto.descricao, fornecedor.nome AS fornecedor,"
                    . "fabricante.nome as fabricante");


            if (!$readSes->getResult()):
                WSErro("Não hà peças cadastradas!", WS_INFOR);
            else:
                foreach ($readSes->getResult() as $key => $ses):
                    extract($ses);


                    $readPecas->ExeRead("produto", "WHERE id = :id", "id={$id}");
                    ?>

                    <tr 
                    <?php
                    if ($key % 2 == 0):
                        echo "class=\"list_prod_blue\" ";
                    elseif ($key % 2 != 0):
                        echo "class=\"list_prod_white\" ";
                    endif;
                    ?>
                        onclick="location.href = 'painel.php?exe=produtos/view&prodid=<?= $id ?>';"
                        >
                        <td><?= $nome; ?></td>
                        <td><?= ($ativo) ? "Sim" : "Não" ?></td>
                        <td><?= $categoria; ?></td>
                        <td><?= $codigo; ?></td>
                        <td><?= $descricao; ?></td>
                        <td><?= $fabricante; ?></td>
                        <td><?= $fornecedor; ?></td>
                    </tr>

                    <?php
                endforeach;
            endif;
            ?>
        </table>
        <div class="clear"></div>
    </section>

    <div class="clear"></div>
</div> <!-- content home -->