<?php ob_start(); ?>
<?php
session_start();
require('../_app/Config.inc.php');

$login = new Login(3);
$logoff = filter_input(INPUT_GET, 'logoff', FILTER_VALIDATE_BOOLEAN);
$getexe = filter_input(INPUT_GET, 'exe', FILTER_DEFAULT);

if (!$login->CheckLogin()):
    unset($_SESSION['userlogin']);
    header('Location: index.php?exe=restrito');
else:
    $userlogin = $_SESSION['userlogin'];
endif;

if ($logoff):
    unset($_SESSION['userlogin']);
    header('Location: index.php?exe=logoff');
endif;
?>
<!DOCTYPE html>
<html lang="pt-br">

    <head>
        <meta charset="UTF-8">
        <title>Estoque</title>
        <!--[if lt IE 9]>
                   <script src="../_cdn/html5.js"></script> 
                <![endif]-->


        <link rel='stylesheet' href="css/bootstrap-reboot.css" type='text/css'>
        <link rel='stylesheet' href="css/tooltip-theme-twipsy.css" type='text/css'>
        <link rel='stylesheet' href="css/bootstrap.css" type='text/css'>
        <link rel="stylesheet" type="text/css" href="plugins/DataTables/datatables.min.css">
        <link rel='stylesheet' href="css/triggers.css" type='text/css'>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">


    </head>

    <body>

        <header>
            <div>
                <nav class="navbar navbar-light" style="background-color: windowframe;">
                    <h1>Pro Estoque</h1>

                    <nav class="navbar navbar-expand-sm navbar-dark bg-dark rounded">
                        <div class="collapse navbar-collapse">
                            <ul  class="navbar-nav mr-auto">
                                <li class="nav-link active"> Olá <?= $userlogin['user_name']; ?> <?= $userlogin['user_lastname']; ?></li>
                                <li><a class="nav-link" href="painel.php?exe=users/profile">Perfíl</a></li>
                                <li><a class="nav-link" href="painel.php?exe=users/users">Usuários</a></li>
                                <li><a class="nav-link" href="painel.php?logoff=true">Sair</a></li>
                            </ul>
                        </div>
                    </nav>
                </nav>
                <nav class="navbar navbar-expand-sm navbar-dark bg-dark">
                    <h1><a  class="navbar-brand" href="painel.php" title="Dasboard">Dasboard</a></h1>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample03" aria-controls="navbarsExample03" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <?php
                    //ATIVA MENU
                    if (isset($getexe)):
                        $linkto = explode('/', $getexe);
                    else:
                        $linkto = array();
                    endif;
                    ?>

                    <div class="collapse navbar-collapse" id="navbarsExample03">
                        <ul class="navbar-nav mr-auto">
                            <!--<li class="nav-item active">-->
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" onclick="return true;" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Estoque</a>
                                <div class="dropdown-menu" aria-labelledby="dropdown03">
                                    <a class="dropdown-item" href="painel.php?exe=estoque/index">Listar</a>
                                    <a class="dropdown-item" href="painel.php?exe=movimentos/index">Movimentos</a>
                                </div>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" onclick="return true;" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Peças</a>
                                <div class="dropdown-menu" aria-labelledby="dropdown03">
                                    <a class="dropdown-item" href="painel.php?exe=produtos/create">Criar</a>
                                    <a class="dropdown-item" href="painel.php?exe=produtos/index">Listar</a>
                                </div>
                            </li>
                            <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Link</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link disabled" href="#">Disabled</a>
                            </li>
                        </ul>
                    </div>
                    </header>

                    <div>
                        <?php
                        //QUERY STRING
                        if (!empty($getexe)):
                            $includepatch = __DIR__ . '/system/' . strip_tags(trim($getexe) . '.php');
                        else:
                            $includepatch = __DIR__ . '/system/home.php';
                        endif;

                        if (file_exists($includepatch)):
                            require_once($includepatch);
                        else:
                            echo "<div class=\"content notfound\">";
                            WSErro("<b>Erro ao incluir tela:</b> Erro ao incluir o controller /{$getexe}.php!", WS_ERROR);
                            echo "</div>";
                        endif;
                        ?>
                    </div> <!-- painel -->		

                    <footer>
                        <a href="" target="_blank" title="PereiraCorp">&copy; PereiraCorp - Todos os Direitos Reservados</a>
                    </footer>

                    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
                    <script src="__jsc/tether.min.js"></script>
                    <script src="__jsc/ckeditor/ckeditor.js"></script>
                    <script src="__jsc/drop.min.js"></script>
                    <script src="__jsc/sorttable.js"></script>
                    <script src="__jsc/tooltip.min.js"></script>
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>

                    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
                    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
                    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>

                    <script>
                                    $(document).ready(function () {
                                        var id = 0;

                                        // Add button functionality

                                        $(document).on("click", "input.add", function () {
                                            id++;
                                            var master = $(this).parents("table");

                                            // Get a new row based on the prototype row
                                            var prot = $(this).closest('table').find(' tbody tr:first').clone();
                                            prot.attr("class", "");
                                            prot.find(".id").attr("value", id);

                                            master.find("tbody").append(prot);
                                        });

                                        // Remove button functionality
                                        $(document).on("click", "table.dynatable input.remove", function () {
                                            $(this).parents("tr:not(:only-child)").remove();
                                        });
                                    });
                    </script>
                   
                    <script>
                        $(document).ready(function () {
                            $('#example').DataTable();
                        });
                        $(function () {
                            $('[data-toggle="tooltip"]').tooltip();
                        });

                        $(function () {
                            $('[data-toggle="popover"]').popover();
                        });


                        $('#minModel').on('show.bs.modal', function (event) {
                            var button = $(event.relatedTarget); // Button that triggered the modal
                            var recipient = button.data('min');
                            // Extract info from data-* attributes
                            // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
                            // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
                            var modal = $(this);
                            modal.find('.modal-body #minEstoque').val(recipient);
                        });

                        CKEDITOR.replace('descricao');


                    </script>

                    </body>


                    </html>