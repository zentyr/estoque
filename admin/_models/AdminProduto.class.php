<?php

/**
 * AdminCategory.class [ MODEL ADMIN ]
 * Responsável oir gerenciar as categorias do sistema no admin!
 * 
 * @copyright (c) 2017, Bráian L.F. Pereira Corp
 */
class AdminProduto {

    private $Data;
    private $ProdId;
    private $Error;
    private $Result;

    //Nome da tabela no banco de dados!    
    const Entity = 'produto';

    public function ExeCreate(array $Data) {
        $this->Data = $Data;

        $this->setData();
        //$this->setName();
        $this->Create();
    }

    public function ExeUpdate($ProdutoId, array $Data) {
        $this->ProdId = (int) $ProdutoId;
        $this->Data = $Data;

//        if (in_array('', $this->Data)):
//            $this->Result = false;
//            $this->Error = ['<b>Erro ao cadastrar:</b> Para cadastrar uma categoria, preencha todos os campos!', WS_ALERT];
//        else:
            $this->setData();
            //$this->setName();
            $this->Update();
//        endif;
    }

    public function ExeDelete($ProdutoId) {
        $this->ProdId = (int) $ProdutoId;
        $this->Delete();
    }

    function getResult() {
        return $this->Result;
    }

    function getError() {
        return $this->Error;
    }

    //PRIVATES
    private function setData() {
		//foreach ($this->Data AS $dados):
		//if(!$dados['descricao']):
			//$this->Data[$dados] = array_map('strip_tags', $this->Data[$dados]);
			//$this->Data[$dados] = array_map('trim', $this->Data[$dados]);
		//endif;
		//endforeach;
        //$this->Data['nome'] = Check::Name($this->Data['nome']);
    }

//    private function setName() {
//        $Where  = (!empty($this->ProdId) ? "id != {$this->ProdId} AND" : '');
//        
//        $readName = new Read;
//        $readName->ExeRead(self::Entity, "WHERE {$Where} category_title = :t", "t={$this->Data['category_title']}");
//        if($readName->getResult()):
//            $this->Data['category_name'] = $this->Data['category_name'] . '-' . $readName->getRowCount();
//
//        endif;
//    }

    private function Create() {
        $Create = new Create;
        $Create->ExeCreate(self::Entity, $this->Data);
        if ($Create->getResult()):
            $this->Result = $Create->getResult();
        //$this->Error = ["<b>Sucesso:</b> A peça $this->Data['nome']} foi cadastrada no sistema!", WS_ACCEPT];
        endif;
    }

    private function Update() {
        $update = new Update;
        $update->ExeUpdate(self::Entity, $this->Data, "WHERE id = :prodid", "prodid={$this->ProdId}");
        if ($update->getResult()):
            $this->Result = true;
            $this->Error = ["<b>Sucesso:</b> A peça {$this->Data['nome']} foi atualizada no sistema!", WS_ACCEPT];
        endif;
    }

    private function Delete() {
        $delete = new Delete;
        $delete->ExeDelete(self::Entity, "WHERE id = :prodid", "prodid={$this->ProdId}");
    }

}
