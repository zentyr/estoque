<?php

/**
 * AdminCategory.class [ MODEL ADMIN ]
 * Responsável oir gerenciar as categorias do sistema no admin!
 * 
 * @copyright (c) 2017, Bráian L.F. Pereira Corp
 */
class AdminMovimento {

    private $Data;
    private $Movimento;
    private $MovimentoItens;
    private $MovId;
    private $Error;
    private $Result;

    //Nome da tabela no banco de dados!    
    const Entity = 'movimentoitem';
    const Entity2 = 'movimento';

    public function ExeCreate(array $Data) {
        $this->Data = $Data;
        $this->setData();
        $this->Create();
    }

    public function ExeUpdate($MovimentoId, array $Data) {
        $this->MovId = (int) $MovimentoId;
        $this->Data = $Data;
        $this->setData();
        $this->Update();
    }

    public function ExeDelete($MovimentoId) {
        $this->MovId = (int) $MovimentoId;
        $this->Delete();
    }

    function getResult() {
        return $this->Result;
    }

    function getError() {
        return $this->Error;
    }

    //PRIVATES
    private function setData() {
//		$this->Data[$dados] = array_map('strip_tags', $this->Data);
//		$this->Data[$dados] = array_map('trim', $this->Data);
        $this->Movimento = array(
            'tipo' => $this->Data['categoriaid'],
            'usuario' => 'braian@pereiracorp.com',
            'data' => date('Y-m-d'));

        $Create = new Create();
        $Create->ExeCreate(self::Entity2, $this->Movimento);
        $this->MovId = $Create->getResult();

        $y = count($this->Data['produtoArray']);
        $this->MovimentoItens = array();
        $produtos = $this->Data['produtoArray'];
        $valores = $this->Data['valor'];
        $quantidades = $this->Data['quantidade'];

        for ($x = 0; $x < $y; $x++) {
            $this->MovimentoItens[$x] = [
                'produtoid' => $produtos[$x],
                'quantidade' => $quantidades[$x],
                'valorunitario' => $valores[$x],
                'movimentoid' => $this->MovId
            ];
        }
    }

    private function Create() {
        $Create = new Create;
        foreach ($this->MovimentoItens as $value) {
            $Create->ExeCreate(self::Entity, $value);
            if ($Create->getResult()):
                $Read = new Read();
                $Read->ExeRead('estoque', "WHERE produtoid = :id", "id={$value['produtoid']}");
                if ($Read->getResult()):
                    $Update = new Update();
                    if ($this->Movimento['tipo']):
                        $Update->ExeUpdate('estoque', ['quantidade' => ($Read->getResult()[0]['quantidade'] + $value['quantidade'])], "WHERE id = :id", "id={$Read->getResult()[0]['id']}");
                    else:
                        $Update->ExeUpdate('estoque', ['quantidade' => ($Read->getResult()[0]['quantidade'] - $value['quantidade'])], "WHERE id = :id", "id={$Read->getResult()[0]['id']}");
                    endif;
                endif;
            else:
                $Create->ExeCreate('estoque', ['produtoid' => $value['produtoid'], 'quantidade' => $value['quantidade']]);
            endif;
        }
    }

    private function Update() {
        $update = new Update;
        $update->ExeUpdate(self::Entity, $this->Data, "WHERE id = :MovId", "MovId={$this->MovId}");
        if ($update->getResult()):
            $this->Result = true;
            $this->Error = ["<b>Sucesso:</b> A peça {$this->Data['nome']} foi atualizada no sistema!", WS_ACCEPT];
        endif;
    }

    private function Delete() {
        $delete = new Delete;
        $delete->ExeDelete(self::Entity, "WHERE id = :MovId", "MovId={$this->MovId}");
    }

}
