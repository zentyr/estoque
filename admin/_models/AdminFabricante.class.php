<?php

/**
 * AdminFabricante.class [ MODEL ADMIN ]
 * Responsável oir gerenciar os fabricantes do sistema no admin!
 * 
 * @copyright (c) 2017, Bráian L.F. Pereira Corp
 */
class AdminFabricante {

    private $Data;
    private $FabrId;
    private $Error;
    private $Result;

    //Nome da tabela no banco de dados!    
    const Entity = 'fabricante';

    public function ExeCreate(array $Data) {

        $data['nome'] = $Data['nomeNovoFabricante'];
        $this->Data = $data;

        if (in_array('', $this->Data)):
            $this->Result = false;
            $this->Error = ['<b>Erro ao cadastrar:</b> Para cadastrar uma categoria, preencha todos os campos!', WS_ALERT];
        elseif ($this->checkData()):
            return $this->Result;
        else:
            $this->setData();
            $this->Create();
        endif;
    }

    public function ExeUpdate($FabricanteId, array $Data) {
        $this->FabrId = (int) $FabricanteId;
        $this->Data = $Data;

//        if (in_array('', $this->Data)):
//            $this->Result = false;
//            $this->Error = ['<b>Erro ao cadastrar:</b> Para cadastrar uma categoria, preencha todos os campos!', WS_ALERT];
//        else:
        $this->setData();
        //$this->setName();
        $this->Update();
//        endif;
    }

    public function ExeDelete($FabricanteId) {
        $this->FabrId = (int) $FabricanteId;
        $this->Delete();
    }

    function getResult() {
        return $this->Result;
    }

    function getError() {
        return $this->Error;
    }

    //PRIVATES
    private function setData() {
        $this->Data = array_map('strip_tags', $this->Data);
        $this->Data = array_map('trim', $this->Data);
        //$this->Data['nome'] = Check::Name($this->Data['nome']);
    }

    private function checkData() {
        $readName = new Read;
        $readName->ExeRead(self::Entity, "WHERE nome = :name", "name={$this->Data['nome']}");
        if ($readName->getResult()):
            $this->Result = $readName->getResult()[0]['id'];
            return true;
        else:
            return false;
        endif;
    }

//    private function setName() {
//        $Where  = (!empty($this->FabrId) ? "id != {$this->FabrId} AND" : '');
//        
//        $readName = new Read;
//        $readName->ExeRead(self::Entity, "WHERE {$Where} nome = :t", "t={$this->Data['nome']}");
//        if(!$readName->getResult()):
//            $this->Data['category_name'] = $this->Data['category_name'] . '-' . $readName->getRowCount();
//
//        endif;
//    }

    private function Create() {
        $Create = new Create;
        $Create->ExeCreate(self::Entity, $this->Data);
        if ($Create->getResult()):
            $this->Result = $Create->getResult();
        //$this->Error = ["<b>Sucesso:</b> A peça $this->Data['nome']} foi cadastrada no sistema!", WS_ACCEPT];
        endif;
    }

    private function Update() {
        $update = new Update;
        $update->ExeUpdate(self::Entity, $this->Data, "WHERE id = :prodid", "prodid={$this->FabrId}");
        if ($update->getResult()):
            $this->Result = true;
            $this->Error = ["<b>Sucesso:</b> A peça {$this->Data['nome']} foi atualizada no sistema!", WS_ACCEPT];
        endif;
    }

    private function Delete() {
        $delete = new Delete;
        $delete->ExeDelete(self::Entity, "WHERE id = :prodid", "prodid={$this->FabrId}");
    }

}
