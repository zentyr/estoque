<?php

/**
 * AdminCategory.class [ MODEL ADMIN ]
 * Responsável oir gerenciar as categorias do sistema no admin!
 * 
 * @copyright (c) 2017, Bráian L.F. Pereira Corp
 */
class AdminEstoque {

    private $Data;
    private $EstId;
    private $Error;
    private $Result;

    //Nome da tabela no banco de dados!    
    const Entity = 'estoque';

    public function ExeCreate(array $Data) {
        $this->Data = $Data;

        $this->setData();
        $this->Create();
    }

    public function ExeUpdate($EstoqueId, array $Data) {
        $this->EstId = (int) $EstoqueId;
        $this->Data = $Data;
        $this->setData();
        $this->Update();
    }

    public function ExeDelete($EstoqueId) {
        $this->EstId = (int) $EstoqueId;
        $this->Delete();
    }

    function getResult() {
        return $this->Result;
    }

    function getError() {
        return $this->Error;
    }

    //PRIVATES
    private function setData() {
		$this->Data = array_map('strip_tags', $this->Data);
		$this->Data = array_map('trim', $this->Data);
    }

    private function Create() {
        $Create = new Create;
        $Create->ExeCreate(self::Entity, $this->Data);
        if ($Create->getResult()):
            $this->Result = $Create->getResult();
        //$this->Error = ["<b>Sucesso:</b> A peça $this->Data['nome']} foi cadastrada no sistema!", WS_ACCEPT];
        endif;
    }

    private function Update() {
        $update = new Update;
        $update->ExeUpdate(self::Entity, $this->Data, "WHERE id = :EstId", "EstId={$this->EstId}");
        if ($update->getResult()):
            $this->Result = true;
            $this->Error = ["<b>Sucesso:</b> foi atualizada no sistema!", WS_ACCEPT];
        endif;
    }

    private function Delete() {
        $delete = new Delete;
        $delete->ExeDelete(self::Entity, "WHERE id = :EstId", "EstId={$this->EstId}");
    }

}
