  <html>
  <head>
    <title>Adicionar linha</title>
     <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
                   
    <script>
      $(document).ready(function(){
        $(document).on('click', 'a.add', function(){
          var row = "<tr><td>Nova coluna</td><td><a href='#' class='add'>Add</a></td><td><a href='#' class='remove'>Remove</a></td></tr>"
          $(this).closest('table').append(row);
        });
        $(document).on('click', 'a.remove', function(){
          $(this).closest('tr').remove();
        });
      });
    </script>
  </head>
  <body>
    <table border="1">
      <tr>
        <td>Coluna</td>
        <td><a href="#" class="add">Add</a></td>
        <td><a href="#" class="remove">Remove</a></td>
      </tr>
    </table>
  </body>
</html>